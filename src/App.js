import React, { Component } from "react";

import Main from "./Components/Main/Main";
import Navbar from "./Components/Navbar/Navbar";

export default class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Main />
      </div>
    );
  }
}
