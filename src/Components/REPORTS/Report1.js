import React from "react";
import { Line } from "react-chartjs-2";

function Report1(props) {
  const options = {
    title: {
      display: true,
      text: "VIEWS REPORT",
    },
    scales: {
      yAxes: [
        {
          ticks: {
            min: 0,
            max: 6,
            stepSize: 1,
          },
        },
      ],
    },
  };

  return (
    <div>
      <div className="charts__left">
        <div className="charts__left__title">
          <div>
            <h1>{props.chartheading}</h1>
            <p>{props.dateofprogram}</p>
          </div>

          <i className="fa fa-inr" aria-hidden="true"></i>
        </div>
        <Line data={props.db} options={options} />
      </div>
    </div>
  );
}

export default Report1;
