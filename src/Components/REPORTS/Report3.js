import React from "react";
import { Bar } from "react-chartjs-2";
function Report3(props) {
  const options = {
    title: {
      display: true,
      text: "PROGRAM REPORT",
    },
    scales: {
      yAxes: [
        {
          ticks: {
            min: 0,
            max: 7,
            stepSize: 1,
          },
        },
      ],
    },
  };

  return (
    <div>
      <div className="charts__left">
        <div className="charts__left__title">
          <div>
            <h1>{props.chartheading}</h1>
            <p>{props.dateofprogram}</p>
          </div>

          <i class="fa fa-bar-chart" aria-hidden="true"></i>
        </div>
        <Bar data={props.db} options={options} />
      </div>
    </div>
  );
}

export default Report3;
