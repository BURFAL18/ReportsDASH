import React from "react";
import { Pie } from "react-chartjs-2";
import ReportPieDB from "../../data/ReportPieDB.json";
// const PieChartDB = ReportPieDB.PieChartDB;
function Report4(props) {
  const options = {
    title: {
      display: true,
      text: "USER REVIEW REPORT",
    },
  };

  return (
    <div className="charts__left">
      <div className="charts__left__title">
        <div>
          <h1>{props.chartheading}</h1>
          <p>{props.dateofprogram}</p>
        </div>

        <i className="fa fa-inr" aria-hidden="true"></i>
      </div>
      <Pie data={ReportPieDB} options={options} />
    </div>
  );
}

export default Report4;
