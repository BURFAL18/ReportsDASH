import React from "react";
import { Pie } from "react-chartjs-2";
function UserReportChart() {
  const options = {
    title: {
      display: true,
      text: "USER REVIEW REPORT",
    },
  };
  const mydata = {
    labels: ["Excellent", "Very Good", "Good", "Fair", "Poor"],
    datasets: [
      {
        label: "Sales for 2020 (in Lakhs) ",
        data: [8, 3, 1, 2, 1],
        backgroundColor: [
          "#adadeb",
          "#ffff00",
          "#b3ff66",
          "#ffcce6",
          "#ccffff",
          "purple",
          "pink",
        ],
      },
    ],
  };
  return (
    <div className="charts__left">
      <div className="charts__left__title">
        <div>
          <h1>Daily Reports</h1>
          <p>26 JAN 2021</p>
        </div>

        <i className="fa fa-inr" aria-hidden="true"></i>
      </div>
      <Pie data={mydata} options={options} />
    </div>
  );
}

export default UserReportChart;
