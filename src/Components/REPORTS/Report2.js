import React from "react";
import { Doughnut } from "react-chartjs-2";
function Report2(props) {
  const options = {
    title: {
      display: true,
      text: "REVIEW REPORT",
    },
  };

  return (
    <div>
      <div className="charts__left">
        <div className="charts__left__title">
          <div>
            <h1>{props.chartheading}</h1>
            <p>{props.dateofprogram}</p>
          </div>
          <i class="fa fa-pie-chart" aria-hidden="true"></i>
        </div>
        <Doughnut data={props.db} options={options} />
      </div>
    </div>
  );
}

export default Report2;
