import "./Navbar.css";

const Navbar = () => {
  return (
    <nav className="navbar">
      <div className="navbar__left">
        <a className="link" href="#">
          REFIER
        </a>
        <a className="active_link" href="#">
          Report
        </a>
      </div>
    </nav>
  );
};

export default Navbar;
