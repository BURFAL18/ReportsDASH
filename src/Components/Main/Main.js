import "./Main.css";
import hello from "../../assets/logo.png";
import React from "react";

import CustomDatePicker from "../DateComp/customRangeDatePicker";
import Review from "../Review/Review";

import Report1 from "../REPORTS/Report1";
import Report2 from "../REPORTS/Report2";
import Report3 from "../REPORTS/Report3";
import Report4 from "../REPORTS/Report4";
import UserReportChart from "../REPORTS/UserReportChart";

import ReportBarDB from "../../data/ReportBarDB.json";
import ReportLineDB from "../../data/ReportLineDB.json";
import ReportPieDB from "../../data/ReportPieDB.json";
import ReportDoughnutDB from "../../data/ReportDoughnutDB.json";

// import DatePicker from "../DatePick/DatePick";

const Main = () => {
  return (
    <main>
      <div className="main__container">
        {/* <!-- MAIN TITLE STARTS HERE --> */}

        <div className="main__title">
          <img src={hello} alt="brandlogo" />
          <div className="main__greeting">
            <h1>Hello User</h1>
            <p>Welcome to your Refier Reports</p>
          </div>
        </div>
        <div style={{display:"flex", marginTop:"1rem"}}> 
          
          <CustomDatePicker />
          <button className="btn1">APPLY</button>
        </div>
        {/* <!-- MAIN TITLE ENDS HERE --> */}

        {/* <!-- MAIN CARDS STARTS HERE --> */}
        <div className="main__cards">
          <div className="card">
            <i
              className="fa fa-user fa-2x text-lightblue"
              aria-hidden="true"
            ></i>
            <div className="card_inner">
              <p className="text-primary-p">Registered Users</p>
              <span className="font-bold text-title">653</span>
            </div>
          </div>

          <div className="card">
            <i className="fa fa-eye fa-2x " aria-hidden="true"></i>
            <div className="card_inner">
              <p className="text-primary-p">Views</p>
              <span className="font-bold text-title">2467</span>
            </div>
          </div>

          <div className="card">
            <i
              className="fa fa-video-camera fa-2x text-red"
              aria-hidden="true"
            ></i>
            <div className="card_inner">
              <p className="text-primary-p">Number of Webinars</p>
              <span className="font-bold text-title">25</span>
            </div>
          </div>

          <div className="card">
            <i
              className="fa fa-thumbs-up fa-2x text-green"
              aria-hidden="true"
            ></i>
            <div className="card_inner">
              <p className="text-primary-p">Number of Likes</p>
              <span className="font-bold text-title">450</span>
            </div>
          </div>
        </div>

        {/* <!-- MAIN CARDS ENDS HERE --> */}

        {/* <!-- CHARTS STARTS HERE --> */}

        <div className="charts">
          <Report1
            chartheading="USER REPORTS "
            dateofprogram="28 JAN 2021"
            db={ReportLineDB}
          />
          <Report3
            chartheading="USER REPORTS "
            dateofprogram="27 JAN 2021"
            db={ReportBarDB}
          />
          <Report4
            chartheading="USER REPORTS "
            dateofprogram="28 JAN 2021"
            db={ReportPieDB}
          />
          <Report2
            chartheading="Trainers Response to queries "
            dateofprogram="29 JAN 2021"
            db={ReportDoughnutDB}
          />
          <Report1
            chartheading="Training Effectiveness "
            dateofprogram="25 JAN 2021"
            db={ReportLineDB}
          />
          <Report3
            chartheading="Program Content "
            dateofprogram="26 JAN 2021"
            db={ReportBarDB}
          />

          <Report2
            chartheading="Overall Program Feedback "
            dateofprogram="27 JAN 2021"
            db={ReportDoughnutDB}
          />
          <UserReportChart />
          {/* <!-- CHARTS ENDS HERE --> */}

          <div className="charts__left">
            <div className="charts__left__title">
              <div>
                <h1> Suggestions or Feedback on Program </h1>
                <h2>Program Name</h2>
                <p> 26 JAN 2021</p>
              </div>
            </div>
            {/* <Review /> */}
            <div className="review_card">
              <div className="review_user">Rohan Singh </div>
              <div className="review_rating">
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
              </div>

              <div className="review_content">
                The Program was really great, got all our doubts clarified!
              </div>
            </div>

            <div className="review_card">
              <div className="review_user">Rahul </div>
              <div className="review_rating">
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
              </div>
              <div className="review_content">
                Can Conduct many sessions on different topics. All fine, the
                time, weekly once schedule, refier platform, the session speaker
                Mr. Raghuraman. Topics covered, spontaneous moves and chats , it
                was truly motivational for us. Thanks to refier! Hoping to have
                many sessions with you.
              </div>
            </div>

            <div className="review_card">
              <div className="review_user">Sam </div>
              <div className="review_rating">
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
              </div>

              <div className="review_content">
                Can extend more session (days) so that we can learn many things
                ! Felt duration is short
              </div>
            </div>
          </div>
          <div className="charts__left">
            <div className="charts__left__title">
              <div>
                <h1>Recommendation</h1>
                <h2>Program Name</h2>
                <p> 26 JAN 2021</p>
              </div>
            </div>
            <Review />
          </div>
        </div>
      </div>
    </main>
  );
};

export default Main;
