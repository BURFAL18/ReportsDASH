import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "../../../node_modules/react-datepicker/dist/react-datepicker.css";

function CustomDatePicker(props) {
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  return (
    <>
      <div>
        <i
          className="fa fa-calendar fa-2x text-lightblue"
          aria-hidden="true"
          style={{ marginRight: "1rem", marginLeft: "2rem" }}
        />
        <DatePicker
          selected={startDate}
          onChange={(date) => setStartDate(date)}
          selectsStart
          startDate={startDate}
          endDate={endDate}
          maxDate={startDate}
          placeholderText="SELECT START DATE"
        />
        <i
          className="fa fa-calendar fa-2x text-red"
          aria-hidden="true"
          style={{ marginRight: "1rem", marginLeft: "2rem" }}
        />
        <DatePicker
          selected={endDate}
          onChange={(date) => setEndDate(date)}
          selectsEnd
          startDate={startDate}
          endDate={endDate}
          minDate={startDate}
          placeholderText="SELECT END DATE"
        />
    
      </div>
    </>
  );
}

export default CustomDatePicker;
