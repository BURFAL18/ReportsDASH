# REPORTS DASH
**DEPLOYED WEBSITE LINK** : https://burfal18.github.io/refier-report-final/

**Alpha Version**: https://burfal18.github.io/ReportsDASH/
## Intro
Report Dash is a  simple Dashboard which can help people to visualize feedback from google form or Json strings into wonderful clean UI .
## STEPS
- Select the Date Range for which reports need to be generated
- Now At top right select Webinar / session for which you need report
## Preview
### Home Page
![image](https://user-images.githubusercontent.com/56060354/128382921-13a3d08b-e33d-43b1-a37f-dd3564c17275.png)
### UI Charts 
![image](https://user-images.githubusercontent.com/56060354/128382278-fd07b129-9398-4c5c-82d5-0e5fd1552302.png)
### Custom Cards for feedbacks and suggestions
![image](https://user-images.githubusercontent.com/56060354/128383055-cc9e7c1a-8197-4665-8ca7-d82328720373.png)

## Tech Used
- ReactJS
- Html / Css
- CHARTJS
- FONTAWESOME
- ReactDatePicker


